# Issue Name

## Agreement
(Put an X between the brackets next to all that apply to this issue)

* [ ] - I'm on the latest release.
* [ ] - I've confirmed this issue hasn't been reported on any other issue.
* [ ] - This issue is specific to this project, and related to its functionality.
* [ ] - this is not a suggestion.

## Description:
[describe the issue you're facing in as much detail as possible]

## Steps to reproduce:

* [Explain the first step]
* [and the second step...]
* [...Third]
* [You get the idea...]

## Aditional Information:
[Add any additional information here that you might have - inculding configuration specifics, system specifics, anyhting that might be abnormal, etc. Do NOT share your token!]