# Contributing

So I hear you want to contribute? Well, first things first, thank you for the consideration - it means a lot to know that someone thinks this is a worthy cause. With that being said, there's a few guidelines I'd like to lay out before we continue. This is purely to make this process easier for all of us.

#### Contents Table

[I have a suggestion](#i-have-a-suggestion)

[I have a question](#i-have-a-question)

[What can I do to help?](#how-can-i-help)
* [Report a bug](#reporting-a-bug)

### I have a suggestion
Really? Sweet! Feel free to open up an issue in gitlabs [issue tracker](https://gitlab.com/LKD70/omxgram/issues).

Please also add the `#suggestion` tag to your issue. This way i can easily filter if things get messy.

### I have a question
Awesome. Feel free to hit me up on telegram: [@LKD70](https://t.me/LKD70)

If for some reason i don't reply, just give me another ping a day or two later, chances are I missed your message. (Don't worry, I don't bite)

## How can I help

### Reporting a bug
So, you've caught yourself a bug eh? I'm sure there's many out there, but good job for finding one through all that spaghetti code.

Before we jumpin to action, let's first check a few things.

**has the bug been reported before?**

It's important to ensure that the bug isn't already reported to cut down on clutter and make my job easier. 

To do this, check the [issue tracker](https://gitlab.com/LKD70/omxgram/issues) and search for anything that might be linked to the bug. If you find something, great! it's already been reported. Feel free to leave a comment sharing your findings.

**Are you on the latest version?**

Bug reports are only useful if the bug still exists. There's a good chance that with every release I'll somehow squat a bug or two while creating a dozen more. Becuase of this, it's important to ensure your copy of the project is up to date.

#### Submitting the issue
Well, that all checks out, you've found a bug in the latest release, it's not been reported before, it's time to do some bug hunting!

To get started, it's important to understand what I need to know before I can proceed to squatting the bug. For this, I've created a simple [issue template](docs/ISSUE_TEMPLATE.md) that I hope you'll follow. 

Once you've written up your issue, please ensure you add any #tags you feel might help to filter the issue better.

Most importantly, thank you for finding the issue, I hope I can find a solution for you ASAP.