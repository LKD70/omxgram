# omxgram

A small 'remote control' for omxplayer using Telegram.

Please note: This was made for personal use. It's very buggy and prone to
errors. That being said, please feel free to [contribute](CONTRIBUTING.md) to the project.

## Installation
1. run `git clone https://gitlab.com/LKD70/omxgram.git` to clone to project
2. run `npm install` from the project folder to install the dependencies
3. open and configure the config.json file
4. run `node index.js` to start the bot


## Configuration
The minimalist configuration has only two settings

#### 1. token
This is your telegram bots token...
You can aquire one from the @botfather

#### 2. default_input_url
this is the default file/url that will be loaded if you /start the bot without
supplying a path/url.


## Usage
The bot is made to be used with only one command. Once you have the bot up and
running you can use the /start command within any chat with the bot. The bot
will send you a remote-control-like keyboard to allow you to control the media
player.

If for any reason the bot runs in to an error (incorrect url/file given, unable
to connnect, etc...) the bot will attempt to ignore it and start from scratch.
Becuase of the limitted error handling, it's recommended to run the bot under
a node process manager like [pm2](https://www.npmjs.com/package/pm2)

#### /start
Syntax: `/start [url/file-path]`


Examples:

| Command	                                                | Description                                                                           |
| :-------------------------------------------------------- | :-----------------------------------------------------------------------------------: |
| `/start`                                                  | player will attempt to start using the default path/url given in the config.json file |
| `/start https://www.rmp-streaming.com/media/bbb-360p.mp4` | player will attempt to play the media at the given URL                                |
| `/start /var/www/html/media/vid.mp4`                      | player will attempt to play the media at the given directory.                         |

## Screenshots
#### Example of the controller as seen in webogram:

![](docs/controller-webogram.png)

#### Example of the controller as seen in Telegram for Android with an amoled theme (YAAAT) applied:

![](docs/controller-android-amoled.png)


## Contributing
Check out the [contributing guide](CONTRIBUTING.md) to see how you can help.