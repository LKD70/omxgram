'use strict';

const
	Markup = require('telegraf/markup'),
	omx = require('node-omxplayer'),
	Telegraf = require('telegraf');

const config = require('config.json');

const 
	{token} = config,
	{default_input_url} = config;

const bot = new Telegraf(token);
let player = omx();

const onPlayerError = ({ reply }, error = null) => {
	if (error === null) {
		console.log('Unknown error...');
	} else if (error && 'message' in error) {
		if (error.message === 'Player is closed.') {
			reply('The player isn\'t currently running. Try to /start it.');
		} else {
			reply('An unknown error occurred: ' + error.message);
		}
	} else {
		reply('ERROR: ' + error);
	}
};

bot.start((ctx) => {
	const message = ctx.message.text.split('/start ');
	const input_url = message.length > 1 ? message[1] : default_input_url;
	try {
		if (player.running) {
			return ctx.reply('Player already running, try /stop first. If you\'ve just closed it, give it a second.');
		}
		try {
			player = omx(input_url, 'hdmi');
		} catch (e) {
			return ctx.reply('Error: ' + e);
		}
		return ctx.replyWithHTML(
			`<b>Now playing:</b> <code>${input_url}</code>`,
			Markup.inlineKeyboard([
				[
					Markup.callbackButton('⏪', 'rewind'),
					Markup.callbackButton('▶ ⏸️', 'pause'),
					Markup.callbackButton('⏩', 'fastFwd')
				],
				[
					Markup.callbackButton('🔉', 'volDown'),
					Markup.callbackButton('🔊', 'volUp')
				],
				[
					Markup.callbackButton('⏪📜', 'prevChapter'),
					Markup.callbackButton('600 ⏪', 'back600'),
					Markup.callbackButton('30 ⏪', 'back30'),
					Markup.callbackButton('⏹️', 'quit'),
					Markup.callbackButton('30 ⏩', 'fwd30'),
					Markup.callbackButton('600 ⏩', 'fwd600'),
					Markup.callbackButton('⏩📜', 'nextChapter')
				],
				[
					Markup.callbackButton('⏮️📖', 'prevSubtitle'),
					Markup.callbackButton('⬇️📖', 'decSubDelay'),
					Markup.callbackButton('📖', 'subtitles'),
					Markup.callbackButton('⬆️📖', 'incSubDelay'),
					Markup.callbackButton('⏭️📖', 'nextSubtitle')
				],
				[
					Markup.callbackButton('⬆️⚡', 'incSpeed'),
					Markup.callbackButton('⬇️⚡', 'decSpeed'),
					Markup.callbackButton('⏮️🎵', 'prevAudio'),
					Markup.callbackButton('⏭️🎵', 'nextAudio')
				]
			]).extra()
		);
	} catch (e) {
		onPlayerError(ctx, e);
		return 0;
	}
});

bot.help(ctx => {
	ctx.reply('A help section? I should probably write one of those...');
});

bot.action('pause', (ctx) => {
	try {
		player.pause();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('volUp', (ctx) => {
	try {
		player.volUp();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('volDown', (ctx) => {
	try {
		player.volDown();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('fastFwd', (ctx) => {
	try {
		player.fastFwd();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('rewind', (ctx) => {
	try {
		player.rewind();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('fwd30', (ctx) => {
	try {
		player.fwd30();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('back30', (ctx) => {
	try {
		player.back30();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('fwd600', (ctx) => {
	try {
		player.fwd600();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('back600', (ctx) => {
	try {
		player.back600();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('quit', (ctx) => {
	try {
		player.quit();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
	ctx.editMessageText('Played stopped. Do you want to /start again?');
});

bot.action('subtitles', (ctx) => {
	try {
		player.subtitles();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('incSpeed', (ctx) => {
	try {
		player.incSpeed();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('decSpeed', (ctx) => {
	try {
		player.decSpeed();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('prevChapter', (ctx) => {
	try {
		player.prevChapter();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('nextChapter', (ctx) => {
	try {
		player.nextChapter();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('prevAudio', (ctx) => {
	try {
		player.prevAudio();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('nextAudio', (ctx) => {
	try {
		player.nextAudio();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('prevSubtitle', (ctx) => {
	try {
		player.prevSubtitle();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('nextSubtitle', (ctx) => {
	try {
		player.nextSubtitle();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('decSubDelay', (ctx) => {
	try {
		player.decSubDelay();
	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.action('incSubDelay', (ctx) => {
	try {
		player.incSubDelay();

	} catch (e) {
		onPlayerError(e);
	}
	ctx.answerCbQuery();
});

bot.startPolling();

